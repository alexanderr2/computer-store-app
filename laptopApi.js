const laptopBuy = document.getElementById("buyLaptop")
const selectLaptop = document.getElementById("laptopSelect")
const infoLaptop = document.getElementById("laptopInfoDiv")
const imgLaptop = document.getElementById("laptopImgDiv")

const bankBalance = document.getElementById("bankBalance");


const urlApi = "https://noroff-komputer-store-api.herokuapp.com/computers";
const urlImg = "https://noroff-komputer-store-api.herokuapp.com/";

async function fetchLaptop() 
    {
        const response = await fetch(
            urlApi,
            {
                method: "GET",
            }
            );
            if (!response.ok) {
                throw new Error(`HTTP error!, status: ${response.status}`);
            }
            const data = await response.json();
            console.log(data);
            for (let i = 0; i < data.length; i++) {
             const computerItem = document.createElement("option")
             computerItem.appendChild(document.createTextNode(data[i].title))
             selectLaptop.appendChild(computerItem);
            }
            
    }
    

    async function fetchDataForLaptops() {
        const response = await fetch(
            urlApi,
            {
                method: 'GET'
            }
        )
        if (!response.ok) {
            throw new Error(`HTTP error!, status: ${response.status}`);
        }


        const data = await response.json();

        let getIndex = document.getElementById('laptopSelect').selectedIndex;
        infoLaptop.innerHTML = "";
        imgLaptop.innerHTML = "";

        const laptopImg = document.createElement("img")
        const imageType = data[getIndex].image;
        laptopImg.src = `${urlImg}${imageType}`;
        imgLaptop.appendChild(laptopImg);

        const laptopTitle = document.createElement("h3")
        laptopTitle.appendChild(document.createTextNode(data[getIndex].title));
        infoLaptop.appendChild(laptopTitle);

        const laptopDescription = document.createElement("h4")
        laptopDescription.appendChild(document.createTextNode(data[getIndex].description));
        infoLaptop.appendChild(laptopDescription);

        const laptopSpecs = document.createElement("p")
        laptopSpecs.appendChild(document.createTextNode(data[getIndex].specs));
        infoLaptop.appendChild(laptopSpecs);

        const laptopPrice = document.createElement("h2")
        laptopPrice.appendChild(document.createTextNode(data[getIndex].price));
        infoLaptop.appendChild(laptopPrice);

        laptopBuy.hidden = false;

    }
    
    titleLaptop = data[getIndex].title;
    priceLaptop = data[getIndex].price;
    currentBalance = bankBalance;
    
    function buyLaptop() {
        if (bankBalance >= priceLaptop) {
            bankBalance = bankBalance - priceLaptop;
            alert(`${titleLaptop} has been purchased and is being sent to you!`);
            currentBalance.innerHTML = bankBalance + " kr";
        }
        else {
            alert("There is not enough funds in your account");
        }     
    }
    

    laptopBuy.hidden = true;
    fetchLaptop();
