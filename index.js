const bankBalance = document.getElementById("bankBalance");
const loanBalance = document.getElementById("loanBalance");
const getLoan = document.getElementById("getLoan");

const workBalance = document.getElementById("workBalance");
const btnBank = document.getElementById("btnBank");
const btnWork = document.getElementById("btnWork");
const loanRepay = document.getElementById("repayLoan");

const laptopBuy = document.getElementById("buyLaptop")
const selectLaptop = document.getElementById("laptopSelect")
const infoLaptop = document.getElementById("laptopInfoDiv")
const imgLaptop = document.getElementById("laptopImgDiv")

// Set of variables with different base values.
let workWages = 0;
let currentBankBalance = 200;
let currentLoanBalance = 0;
let remainingLoan = 0;

// Adds 100 to workWages and updates the html element.
function getSalary() {
    workWages = workWages + 100;
    workBalance.innerHTML = workWages + " kr";
}

// Checks if there is a loan greater then 0, if so it adds 10% interest every time money is deposited into the bank
function depositBankBalance() {
    if (currentLoanBalance > 0) {
        let interest = workWages * 0.1;
        workWages = workWages - interest;
        if (currentLoanBalance < interest) {
            workWages = workWages + (interest - currentLoanBalance);
        }
        else {
            currentLoanBalance = currentLoanBalance - interest;
            workWages = workWages - interest;
        }
        loanBalance.innerHTML = currentLoanBalance + " kr";
    }


    currentBankBalance = currentBankBalance + workWages;
    workWages = 0;
    bankBalance.innerHTML = currentBankBalance + " kr";
    workBalance.innerHTML = workWages + " kr";
}

// Prompts user to type loan amount, if bigger then x2 alert user. Otherwise take loan request and add to bank balance and update html element.
function createLoan() {
    const requestLoan = prompt("Please enter the amount you want to loan.")
    if (requestLoan > (currentBankBalance * 2)) {
        alert("The amount you have entered is twice as big as your current balance!");
    }
    else if (requestLoan !== null) {
        currentBankBalance = currentBankBalance + parseInt(requestLoan);
        bankBalance.innerHTML = currentBankBalance + " kr";
        currentLoanBalance = requestLoan;
        loanBalance.innerHTML = requestLoan + " kr";
        getLoan.disabled = true;
        loanRepay.hidden = false;
    }
}

// Function for repaying the loan via repay button, compares workWages and LoanBalance
function repayLoan() {
    if (workWages >= currentLoanBalance) {
        remainingLoan = workWages - currentLoanBalance;
        currentBankBalance = currentBankBalance + remainingLoan;
        workWages = remainingLoan;
        currentLoanBalance = 0;
        workWages = 0;
        getLoan.disabled = false;
        loanRepay.hidden = true;
    }
    else if (workWages < currentLoanBalance) {
        remainingLoan = currentLoanBalance - workWages;
        currentLoanBalance = remainingLoan;
        workWages = 0;
    }
    workBalance.innerHTML = workWages + " kr";
    loanBalance.innerHTML = currentLoanBalance + " kr";
    bankBalance.innerHTML = currentBankBalance + " kr";
}

loanRepay.hidden = true;


// Api fetch to Get laptop title and create Option node to display title.

const urlApi = "https://noroff-komputer-store-api.herokuapp.com/computers";
const urlImg = "https://noroff-komputer-store-api.herokuapp.com/";

async function fetchLaptop() 
    {
        const response = await fetch(
            urlApi,
            {
                method: "GET",
            }
            );
            if (!response.ok) {
                throw new Error(`HTTP error!, status: ${response.status}`);
            }
            const data = await response.json();
            console.log(data);
            for (let i = 0; i < data.length; i++) {
             const computerItem = document.createElement("option")
             computerItem.appendChild(document.createTextNode(data[i].title))
             selectLaptop.appendChild(computerItem);
            }
            
    }
    
// Fetches info from Api and creates html elements for new div that shows info and price for selected laptop.
    async function fetchDataForLaptops() {
        const response = await fetch(
            urlApi,
            {
                method: 'GET'
            }
        )
        if (!response.ok) {
            throw new Error(`HTTP error!, status: ${response.status}`);
        }


        const data = await response.json();

        let getIndex = document.getElementById('laptopSelect').selectedIndex;
        infoLaptop.innerHTML = "";
        imgLaptop.innerHTML = "";

        const laptopImg = document.createElement("img")
        const imageType = data[getIndex].image;
        laptopImg.src = `${urlImg}${imageType}`;
        imgLaptop.appendChild(laptopImg);

        const laptopTitle = document.createElement("h3")
        laptopTitle.appendChild(document.createTextNode(data[getIndex].title));
        infoLaptop.appendChild(laptopTitle);

        const laptopDescription = document.createElement("h4")
        laptopDescription.appendChild(document.createTextNode(data[getIndex].description));
        infoLaptop.appendChild(laptopDescription);

        const laptopSpecs = document.createElement("p")
        laptopSpecs.appendChild(document.createTextNode(data[getIndex].specs));
        infoLaptop.appendChild(laptopSpecs);

        const laptopPrice = document.createElement("h2")
        laptopPrice.appendChild(document.createTextNode(data[getIndex].price + " kr"));
        infoLaptop.appendChild(laptopPrice);

        laptopBuy.hidden = false;

        titleLaptop = data[getIndex].title;
        priceLaptop = data[getIndex].price;
    }
    
    // Function for buying laptop, compares bank balance with price and updates html element.
    function buyLaptop() {
        if (currentBankBalance >= priceLaptop) {
            currentBankBalance = currentBankBalance - priceLaptop;
            alert(`${titleLaptop} has now been purchased!`);
            bankBalance.innerHTML = currentBankBalance + " kr";
        }
        else {
            alert("There is not enough funds in your account to make this purchase!");
        }     
    }
    

    laptopBuy.hidden = true;
    fetchLaptop();

// Adds eventlisteners to buttons and runs functions.
laptopBuy.addEventListener('click', buyLaptop);
loanRepay.addEventListener('click', repayLoan);
getLoan.addEventListener('click', createLoan);
btnBank.addEventListener('click', depositBankBalance);
btnWork.addEventListener('click', getSalary);